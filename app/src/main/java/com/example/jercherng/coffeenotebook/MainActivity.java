package com.example.jercherng.coffeenotebook;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
//    private CoffeeNotes expert = new CoffeeNotes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

//    public void setImage(int coffeeName) {
//        String imageName[] = {"bella_donovan", "beta_blend", "blend_box", "giant_steps", "three_africans"};
//        ImageView imageView = (ImageView) findViewById(R.id.coffeeImages);
//
//        int imageResource = getResources().getIdentifier("@drawable/" + imageName[coffeeName] + ".jpg", null, getPackageName());
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imageResource);
//        imageView.setImageBitmap(bitmap);
//    }

    //Call when the button gets clicked
    public void onClickCoffeeNames(View view) {

        // it's getting all the info from activity_main.xml

        /* Get a reference to the Spinner
         * creating a spinner object. Once that's done, we can use the findViewById function to
         * identify the names
         */
        Spinner coffeeNames = (Spinner) findViewById(R.id.coffeeNames);
        // Get the selected item in the Spinner
        String coffeeName = String.valueOf(coffeeNames.getSelectedItem());

        /* I can't do it like this because I'm basically taking what is already there
         * and putting it back.
         */

        TextView descriptionBox = (TextView) findViewById(R.id.descriptionBox);
//        String coffeeDescription = String.valueOf(coffeeDescriptions.getSelectedItem());
//        coffeeDescription.setText();

        /* Instead, what I should do is create a class
         * 1. Class shall have name
         * 2. Shall have description
         * 3. Future feature: read from file and setting the value.
         */
        String coffeeDescription = Coffee.checkDescription(coffeeName);

//        setImage(coffeeNames.getSelectedItemPosition());
        String imageName[] = {"bella_donovan", "beta_blend", "blend_box", "giant_steps", "three_africans"};
        ImageView imageView = (ImageView) findViewById(R.id.coffeeImages);
//
//        int imageResource = getResources().getIdentifier("drawable/" + imageName[coffeeNames.getSelectedItemPosition()] + ".jpg", null, getPackageName());
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imageResource);
//        imageView.setImageBitmap(bitmap);

        Resources res = this.getResources();
        int resID = res.getIdentifier(imageName[coffeeNames.getSelectedItemPosition()], "drawable", this.getPackageName());
        imageView.setImageResource(resID);

        descriptionBox.setText(coffeeDescription);
    }
}