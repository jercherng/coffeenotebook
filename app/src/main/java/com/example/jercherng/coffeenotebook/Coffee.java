package com.example.jercherng.coffeenotebook;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JerCherng on 2/3/2016.
 */
public enum Coffee {
    COFFEEONE  ("Bella Donovan", "The Bella is the wool sweater of our blends -- warm, comforting, familiar. Wild and citrusy organic African paired with earthy organic Indonesian makes for a vivid and fairly complex Moka/Java blend. It seems to weather the rigors of the automatic drip machine as well. This is, perhaps, why it's our most popular blend. It is also on the darker side: nice and thick without being inelegant. Bella stands up to milk or cream well, and is easily enjoyed black."),
    COFFEETWO  ("Beta Blend", "While many of our drip coffee blends are noteworthy for their low-toned sturdiness, Beta Blend is a bit of a different animal – glassy, complex, floral. A collaborative project between our Sourcing and Digital teams, it offers a daintier counterpoint to our Three Africans, Bella Donovan, and Giant Steps. It is available for online purchase exclusively."),
    COFFEETHREE("Blend Box", "Our blends are carefully chosen from two or three coffees for their harmonious relationship in the cup. Roasted with desired flavor profiles in mind, each blend combines varying regions, climates, and processing methods to arrive at something that’s as comforting as an old friend, but flushed with the excitement of first love"),
    COFFEEFOUR ("Giant Steps", "A blend of organic coffee that is quite dark and chocolaty. Maybe a bit more one-dimensional than the Bella Donovan, but it is a good dimension. Giant Steps is delightfully fudgy and thick in a French press or drip pot, and stands up to cream quite well. If it were any heavier-bodied, you could pour it on pancakes."),
    COFFEEFIVE ("Three Africans", "This coffee is generally a blend of Congolese and two different Ethiopians coffees. Together, these components produce a big, chocolaty aroma and excel in just about every prep method, save espresso and siphon. Unlike some of our fancy-shmancy single origins, this blend has a very easy-to-like personality, good body, unthreatening complexity, and a reasonably clean aftertaste. The coffees—two Ethiopians, one dry-processed and one wet processed; and a wet-processed Congolese—leave subtle imprints of dried blueberries and cardamom. A fairly dark roast, this inclusive blend will take milk or cream well. Some say damn well.");

    private final String name;   // in kilograms
    private final String description; // in meters
    Coffee(String name, String description) {
        this.name = name;
        this.description = description;
    }
    private String getName() { return name; }
    private String getDescription() { return description; }

    public static String checkDescription(String coffeeName) {
        String coffeeDescr = "";
//        private static final String TAG = MainActivity.enum.Coffeeaa;

        for (Coffee c : Coffee.values())
            if (coffeeName.equals(c.getName()))
                coffeeDescr = c.getDescription();

        return coffeeDescr;
    }
}
